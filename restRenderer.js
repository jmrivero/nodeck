var dust = require('dust'),
	codegen = require('./codegen.js'),
	fs = require('fs');	
	
var operationMap = {
	"create" : "POST",
	"read" : "GET",
	"delete" : "DELETE",
	"associate": "PUT",
	"dissociate": "DELETE"
}
	
module.exports = {
	
	render: function(mockupddModel, callback) {
		var self = this;
		mockupddModel.classes.forEach(function(klass) {
			klass.operations = self.operationsToArray(klass.operations);
			if (klass.associateOperations) {
				klass.associateOperations = self.associateOperationsToArray(klass.associateOperations);
			}
		});
		fs.readFile('codegen/templates/rest.tl', function(err, template) {
			dust.loadSource(dust.compile(template.toString(), 'rest'));
			dust.render('rest', mockupddModel, function(err, out) {
				callback(out);
			});
		});
	},
	
	operationsToArray: function(operationsObject) {
		var operations = [];
		for (operation in operationsObject) {
			operations.push(operationMap[operation]);
		}
		return operations;
	},
	
	associateOperationsToArray: function(operationsObject) {
		var operations = [];
		for (property in operationsObject) {
			var operationsForAssociation = [];
			operations.push({property: property, operations: operationsForAssociation});
			for (operation in operationsObject[property]) {
				operationsForAssociation.push(operationMap[operation]);
			}
		}
		return operations;
	}

}

