var xml2js = require('xml2js'),
	fs = require('fs'),
	async = require('async');

var MockupReader = function() {}
	
module.exports = MockupReader;	

MockupReader.prototype.readMockups = function(path, tagParser, logger, callback) {
	this.readMockupFiles(path, function(mockups) {
		callback(this.processMockups(mockups, tagParser, logger));
	})
};
	
MockupReader.prototype.getFileExtension = function() {
	throw  "Must be implemented";
}

MockupReader.prototype.processMockups = function(mockups, tagParser, logger) {
	throw  "Must be implemented";
}

MockupReader.prototype.parseMockupContent = function(mockupContent, mockupName, callback)
	throw  "Must be implemented";
}

MockupReader.prototype.processReadMockups = function(mockups, tagParser, logger, callback) {
	var processedMockups = [];
	mockups.forEach(function(mockup) {
		this.parseMockupContent(mockup.content, mockup.name, function(content) {
			processedMockups.push(content);
		});
	});
	callback(this.processMockups(mockups, tagParser, logger));
}

MockupReader.prototype.readMockups = function(path, tagParser, logger, callback) {
	this.readMockupFiles(path, function(mockups) {
		callback(this.processMockups(mockups, tagParser, logger));
	})
}

MockupReader.prototype.processReadMockups = function(mockups, tagParser, logger, callback) {
	var processedMockups = [];
	var self = this;
	mockups.forEach(function(mockup) {
		self.parseMockupContent(mockup.content, mockup.name, function(content) {
			processedMockups.push(content);
		});
	});
	callback(this.processMockups(mockups, tagParser, logger));
}

MockupReader.prototype.readMockupFiles = function(path, callback) {
	var self = this;
	fs.readdir(path, function(err, files) {
		var mockups = [];
		var fileReads = [];
		files.forEach(function(file) {
			if (file.substring(file.length - 5) == this.getFileExtension()) {
				fileReads.push(function(callback) {
					fs.readFile(path + '/' + file, function(err, data) {
						self.parseMockupContent(data, file.substring(0, file.length - 5), function(parsedMockup) {
							mockups.push(parsedMockup);
							callback();
						});
					});
				});
			}
		});
		async.series(fileReads, function(err, result) {
			callback(mockups);
		});
	});
}

function parseTags(tagParser, tagDescription,  mockupName, logger, callback) {
	try {
		callback(tagParser.parse(tagDescription));
	} catch (e) {
		logger.addEntry("Parsing error in tag: \"" + tagDescription + ", mockup \"" + mockupName + "\" - " + e.message);
	}
}