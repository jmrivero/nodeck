var dust = require('dust'),
	fs = require('fs');	
	
module.exports = {
	
	render: function(mockupddModel, callback) {
		dust.loadSource(dust.compile(fs.readFileSync('codegen/templates/widgets.tl').toString(), 'widgetsTemplate'));
		dust.loadSource(dust.compile(fs.readFileSync('codegen/templates/html.tl').toString(), 'htmlTemplate'));
		mockupddModel.pages.forEach(function(page) {
			page.getWidgetsRecursively().forEach(function(widget) {
				if (widget.type) {
					// do not redefine in this case
					if (!widget.isRepetition) {
						widget["is" + widget.type] = true;
					}
				} else {
					widget["isOther"] = true;
				}
			});
			page.allWidgets = page.getWidgetsRecursively();
		});
		dust.render('htmlTemplate', mockupddModel, function(err, out) {
			console.log(err);
			callback(out);
		});
	}

}

