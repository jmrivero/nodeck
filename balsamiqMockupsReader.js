var xml2js = require('xml2js'),
	fs = require('fs'),
	async = require('async'),
	Widget = require('./model/Widget.js');

var parser = new xml2js.Parser();

module.exports = {
	readMockups: function(path, tagParser, logger, callback) {
		readBalsamiqMockupsFiles(path, function(mockups) {
			callback(processBalsamiqMockups(mockups, tagParser, logger));
		})
	},
	
	processReadMockups: function(mockups, tagParser, logger, callback) {
		var processedMockups = [];
		mockups.forEach(function(mockup) {
			parseMockupContent(mockup.content, mockup.name, function(content) {
				processedMockups.push(content);
			});
		});
		callback(processBalsamiqMockups(mockups, tagParser, logger));
	}
}

var controlTypeMaps = {
	'Link':'Link',
	'Canvas':'Panel',
	'Button':'Button',
	'Label':'Label',
	'TextInput':'TextBox'
}

var tagProcessors = {
	'RoundButton': function(control) {
		return control.controlProperties[0].text[0];
	}
}

function readBalsamiqMockupsFiles(path, callback) {
	fs.readdir(path, function(err, files) {
		var mockups = [];
		var fileReads = [];
		files.forEach(function(file) {
			if (file.substring(file.length - 5) == ".bmml") {
				fileReads.push(function(callback) {
					fs.readFile(path + '/' + file, function(err, data) {
						parseMockupContent(data, file.substring(0, file.length - 5), function(parsedMockup) {
							mockups.push(parsedMockup);
							callback();
						});
					});
				});
			}
		});
		async.series(fileReads, function(err, result) {
			callback(mockups);
		});
	});
}

function parseMockupContent(mockupContent, mockupName, callback) {
	parser.parseString(mockupContent, function(err, data) {
		callback({name: mockupName, content: data});
	});
}

function processBalsamiqMockups(mockups, tagParser, logger) {
	var tags = [];
	var pages = [];
	mockups.forEach(function(mockup) {
		var widgets = [];
		var page = {name: mockup.name, widgets: [], tags: []};
		mockup.content.mockup.controls[0].control.forEach(function(control) {
			var controlType = control.$.controlTypeID.split('::')[1];
			if (tagProcessors[controlType]) {
				parseTags(tagParser, unescape(unescape(tagProcessors[controlType](control))), mockup.name, logger, function(parsedTags) {
					parsedTags.forEach(function(parsedTag) {
						page.tags.push(parsedTag);
						tags.push(parsedTag);
					});
				});
			} else {
				var widget = new Widget({
					type: controlTypeMaps[controlType],
					id: control.$.controlID, 
					x: parseInt(control.$.x, 10), 
					y: parseInt(control.$.y, 10), 
					width: parseInt(control.$.w > -1 ? control.$.w : control.$.measuredW),
					height: parseInt(control.$.h > -1 ? control.$.h : control.$.measuredH)});
				if (control.controlProperties && control.controlProperties[0].text) {
					widget.label = unescape(unescape(control.controlProperties[0].text[0]));
				}
				if (control.controlProperties && control.controlProperties[0].customData) {
					var tagText = unescape(unescape(control.controlProperties[0].customData[0]));
						parseTags(tagParser, tagText, mockup.name, logger, function(parsedTags) {
							parsedTags.forEach(function(parsedTag) {
								page.tags.push(parsedTag);
								tags.push(parsedTag);
								widget.tags.push(parsedTag);
								parsedTag.widget = widget;
							});
						});
				}
				page.widgets.push(widget);
			}
		})
		pages.push(page);
	});
	return {
		pages: pages,
		tags: tags
	}
}

function parseTags(tagParser, tagDescription,  mockupName, logger, callback) {
	try {
		callback(tagParser.parse(tagDescription));
	} catch (e) {
		logger.addEntry("Parsing error in tag: \"" + tagDescription + ", mockup \"" + mockupName + "\" - " + e.message);
	}
}