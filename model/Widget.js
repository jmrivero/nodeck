var _ = require("underscore");

var Widget = function(parameters) {
	this.parent = null;
	this.specs = {};
	this.widgets = [];
	this.tags = [];
	_.extend(this, parameters);

}

Widget.prototype.getParents = function(){
	var parents = [];
	var parent = this.parent;
	while (parent) {
		parents.push(parent);
		parent = parent.parent;
	}
	return parents;
}	

Widget.prototype.getWidgetsRecursively = function() {
	var widgets = this.widgets.concat();
	widgets.forEach(function(widget) {
		widgets = widgets.concat(widget.getWidgetsRecursively());
	});
	return widgets;
}

module.exports = Widget;