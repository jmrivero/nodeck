var eyes = require('eyes');
	Widget = require('./model/Widget.js'),
	_ = require('underscore');

module.exports = {

	process: function(pages) {
		var self = this;
		for (i in pages) {
			pages[i] = new Widget(pages[i]);
		}
		pages.forEach(function(page) {
			var pageWidget = new Widget({id: 'page_' + page.name, pageName: page.name, x: 0, y: 0, height: 0, width: 0});
			page.widgets.forEach(function(widget) {
				var parentWidgets = page.widgets.filter(function(w) { return self.widgetIsContainedBy(widget, w) && w.id != widget.id; });
				parentWidgets.sort(function(w1, w2) {
					return self.computeInnerDistance(widget, w1) < self.computeInnerDistance(widget, w2) ? -1 : 1;
				});
				if (parentWidgets.length > 0) {
					var parentWidget = parentWidgets[0];
					if (!parentWidget.widgets) {
						parentWidget.widgets = [];
					}
					widget.parent = parentWidget;
					parentWidget.widgets.push(widget);
				} else {
					widget.parent = pageWidget;
				}
			});
			// computes widgets offsets instead of their absolute position
			page.widgets.forEach(function(widget) {
				var parentWidget = widget.parent;
				while (parentWidget != pageWidget) {
					widget.x = widget.x - parentWidget.x;
					widget.y = widget.y - parentWidget.y;
					parentWidget = parentWidget.parent;
				}
			});
			page.widgets = _.reject(page.widgets, function(widget) {return widget.parent != pageWidget });
		});
	},
	
	pointIsInsideWidget: function(x, y, widget) {
		return widget.x <= x && x <= widget.x + widget.width &&
			widget.y <= y && y <= widget.y + widget.height;
	},
	
	widgetIsContainedBy: function(widget1, widget2) {
		return this.pointIsInsideWidget(widget1.x, widget1.y, widget2) && 
			this.pointIsInsideWidget(widget1.x + widget1.width, widget1.y + widget1.height, widget2)
	},
	
	computeInnerDistance: function(widget1, widget2) {
		return Math.abs(widget1.x - widget2.x) + Math.abs(widget1.width - widget2.width) +
			Math.abs(widget1.y - widget2.y) + Math.abs(widget1.height - widget2.height)
	}
	
};