var eyes = require('eyes'),
	tagParser = require('./tagParser.js'),
	tagProcessor = require('./tagProcessor.js'),
	codegen = require('./codegen.js'),
	classRenderer = require('./classRenderer.js'),
	htmlRenderer = require('./htmlRenderer.js'),
	restRenderer = require('./restRenderer.js'),
	widgetProcessor = require('./widgetProcessor.js'),
	mockupReader = require('./pencilMockupsReader.js') //require('./balsamiqMockupsReader.js'),
	async = require('async');

function processMockupResult(result, logger) {
	var freshTagProcessor = new tagProcessor.TagProcessor();
	widgetProcessor.process(result.pages);
	var mockupddModel = freshTagProcessor.processTags(result.tags, result.pages, logger);
	mockupddModel.pages = result.pages;
	return mockupddModel;
};

function executeCodeGeneration(mockupddModel, classOutputDirectory, htmlOutputDirectory, restOutputDirectory, callback) {
	async.parallel(
	[function(next) {
		if (classOutputDirectory) {
			classRenderer.render(mockupddModel, function(out) {
				codegen.splitFiles(out, classOutputDirectory);
				next(null, 0);
			});
		} else {
			next(null, 0);
		}
	}, function(next) {
		if (htmlOutputDirectory) {
			debugger;
			htmlRenderer.render(mockupddModel, function(out) {
				codegen.splitFiles(out, htmlOutputDirectory);
				next(null, 1);
			});
		} else {
			next(null, 1);
		}
	}, function(next) {
		if (restOutputDirectory) {
			restRenderer.render(mockupddModel, function(out) {
				codegen.splitFiles(out, restOutputDirectory);
				next(null, 1);
			});
		} else {
			next(null, 1);
		}
	}],
	function(err, results) {
		if (callback) {
			callback();
		}
	});
}
	
module.exports = {

	processMockups: function(mockupsDirectory, classOutputDirectory, htmlOutputDirectory, restOutputDirectory, logger, callback) {
		mockupReader.readMockups(mockupsDirectory, tagParser, logger, function(result) {
			debugger;
			executeCodeGeneration(processMockupResult(result, logger), classOutputDirectory, htmlOutputDirectory, restOutputDirectory, callback);
		});
	},
	
	processReadMockups: function(readMockups, classOutputDirectory, htmlOutputDirectory, restOutputDirectory, logger, callback) {
		mockupReader.processReadMockups(readMockups, tagParser, logger, function(result) {
			debugger;
			executeCodeGeneration(processMockupResult(result, logger), classOutputDirectory, htmlOutputDirectory, restOutputDirectory, callback);
		});
	}

}




