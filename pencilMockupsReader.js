var xml2js = require('xml2js'),
	fs = require('fs'),
	async = require('async'),
	Widget = require('./model/Widget.js');

var parser = new xml2js.Parser();

module.exports = {
	readMockups: function(path, tagParser, logger, callback) {
		readPencilMockupsFiles(path, function(mockups) {
			callback(processPencilMockups(mockups, tagParser, logger));
		})
	},
	
	processReadMockups: function(mockups, tagParser, logger, callback) {
		var processedMockups = [];
		mockups.forEach(function(mockup) {
			parseMockupContent(mockup.content, mockup.name, function(content) {
				processedMockups.push(content);
			});
		});
		callback(processPencilMockups(mockups, tagParser, logger));
	}
}

function readPencilMockupsFiles(path, callback) {
	fs.readdir(path, function(err, files) {
		var mockups = [];
		var fileReads = [];
		files.forEach(function(file) {
			if (file.substring(file.length - 3) == ".ep") {
				fileReads.push(function(callback) {
					fs.readFile(path + '/' + file, function(err, data) {
						parseMockupContent(data, file.substring(0, file.length - 3), function(parsedMockup) {
							mockups.push(parsedMockup);
							callback();
						});
					});
				});
			}
		});
		async.series(fileReads, function(err, result) {
			callback(mockups);
		});
	});
}

function parseMockupContent(mockupContent, mockupName, callback) {
	parser.parseString(mockupContent, function(err, data) {
		callback({name: mockupName, content: data});
	});
}

function processPencilMockups(mockups, tagParser, logger) {
	var tags = [];
	var pages = [];
	mockups.forEach(function(mockup) {
		var widgets = [];
		var page = {name: mockup.name, widgets: [], tags: []};
		mockup.content.Document.Pages[0].Page[0].Content[0].g.forEach(function(element) {
			if (element.$["p:def"] == 'Evolus.Common:Balloon') {
				parseTags(tagParser, element.foreignObject[0].div[0]._, mockup.name, logger, function(parsedTags) {
					parsedTags.forEach(function(parsedTag) {
						page.tags.push(parsedTag);
						tags.push(parsedTag);
					});
				});
			}
		});
		pages.push(page);
	});
	return {
		pages: pages,
		tags: tags
	}
}

function parseTags(tagParser, tagDescription,  mockupName, logger, callback) {
	try {
		callback(tagParser.parse(tagDescription));
	} catch (e) {
		logger.addEntry("Parsing error in tag: \"" + tagDescription + ", mockup \"" + mockupName + "\" - " + e.message);
	}
}