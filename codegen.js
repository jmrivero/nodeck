var fs = require('fs');

module.exports = {
	
	splitFiles: function(source, outputDir) {
		var parts = source.split(/(\[\[|\]\])/);
		for (var partIndex = 0; partIndex < parts.length;) {
			if (parts[partIndex] == "[[") {
				fs.writeFileSync(outputDir + '/' + parts[partIndex + 1], parts[partIndex + 3]);
				partIndex = partIndex + 4;
			} else {			
				partIndex++;
			}
		}
	},
	
	firstToUpper: function(word) {
		return word.substring(1, 0).toUpperCase() + word.substring(1);
	},
	
	makeUpper: function(object, property) {
		object[property + 'Upper'] = this.firstToUpper(object[property]);
	}

}