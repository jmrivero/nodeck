var nodeck = require('../nodeck.js')
	express = require('express'),
	fs = require('fs'),
	dust = require('dust'),
	async = require('async'),
	AdmZip = require('adm-zip'),
	logger = require('../logger.js'),
	Zip = require('node-native-zip');
	
var app = express();

app.use(express.bodyParser());

async.parallel([
	function(callback) {
		fs.readFile('web/pages/uploadMockup.html', function(err, template) {			
			dust.loadSource(dust.compile(template.toString(), 'mockupUpload'));
			callback();
		});
	},
	function(callback) {
		fs.readFile('web/pages/mockupsUploaded.html', function(err, template) {
			dust.loadSource(dust.compile(template.toString(), 'mockupsUploaded'));
			callback();
		});
	}
], function() {

	app.get('/', function(req, res){
		dust.render('mockupUpload', {}, function(err, out) {
			res.send(out);
		});
	});
	
	app.get('/results/:id', function(req, res){
		var directoryName = 'web/work/' + new Date().getTime();
		res.setHeader('Content-Disposition', 'attachment; filename=result.zip');
		res.setHeader('Content-Type', 'application/zip');
		res.send(fs.readFileSync('web/work/' + req.params.id + "/result.zip"));
	});

 	app.use('/images', express.static('web/resources/images'));
	app.use('/css', express.static('web/resources/css'));
	app.use('/js', express.static('web/resources/js'));
	
	app.post('/', function(req, res) {
		var newLogger = new logger.Logger();
		var model = {};
		if (req.files.mockupFile) {
			model.filename = req.files.mockupFile.filename;
		}
		model.files = [];
		var zipFile = new AdmZip(req.files.mockupFile.path);
		zipFile.getEntries().forEach(function (entry) {
			model.files.push(entry);
		});
		var resultId = new Date().getTime();
		var directoryName = 'web/work/' + resultId;
		fs.mkdirSync(directoryName);
		fs.mkdirSync(directoryName + '/mockups');
		fs.mkdirSync(directoryName + '/html');
		fs.mkdirSync(directoryName + '/classes');
		fs.mkdirSync(directoryName + '/rest');
		zipFile.extractAllTo(directoryName + '/mockups');
		nodeck.processMockups(directoryName + '/mockups', directoryName + '/classes', directoryName + '/html', directoryName + '/rest', newLogger, function() {
			var resultZip = new Zip();
			var files = [];
				fs.readdirSync(directoryName + '/html').forEach(function(filename) {
					files.push({name: 'html/' + filename, path: directoryName + '/html/' + filename});
				});
				fs.readdirSync('codegen/resources/html').forEach(function(filename) {
					files.push({name: 'html/lib/' + filename, path: 'codegen/resources/html/' + filename});
				});
				fs.readdirSync(directoryName + '/classes').forEach(function(filename) {
					files.push({name: 'classes/' + filename, path: directoryName + '/classes/' + filename});
				});
				fs.readdirSync(directoryName + '/rest').forEach(function(filename) {
					files.push({name: 'rest/' + filename, path: directoryName + '/rest/' + filename});
				});
				resultZip.addFiles(files, function(err) {
					fs.writeFileSync(directoryName + "/result.zip", resultZip.toBuffer());
					newLogger.addEntry("-----");
					newLogger.addEntry("Processing finished");
					dust.render('mockupsUploaded', {logEntries: newLogger.getEntries(), fileLink: 'results/' + resultId}, function(err, out) {
						res.send(out);
					});
					/*
					res.setHeader('Content-Disposition', 'attachment; filename=result.zip');
					res.setHeader('Content-Type', 'application/zip');
					fs.writeFileSync(directoryName + "/result.zip", resultZip.toBuffer());
					res.send(resultZip.toBuffer());
					*/
				});
		});
		/*
		dust.render('mockupUpload', model, function(err, out) {
			res.send(out);
		});
		*/
	});
	app.listen(3000);
});



