function Logger() {
	this._entries = [];
}

Logger.prototype.addEntry = function(content) {
	this._entries.push({timestamp: new Date().getTime(), content: content});
}

Logger.prototype.getEntries = function(content) {
	return this._entries.concat();
}

module.exports = {
	Logger: Logger
}