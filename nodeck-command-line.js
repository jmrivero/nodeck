var nodeck = require('./nodeck.js'), eyes = require('eyes');

console.log('nodeck 0.1 - Mockup processor');
console.log('Jose Matias Rivero - mrivero@lifia.info.unlp.edu.ar');
console.log('--');

var mockupsDirectory = process.argv[2];
var classOutputDirectory = process.argv[3];
var htmlOutputDirectory = process.argv[4];

nodeck.processMockups(mockupsDirectory, classOutputDirectory, htmlOutputDirectory);

console.log('Process finished');
console.log('--');