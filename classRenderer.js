var dust = require('dust'),
	inspect = require('eyes').inspector({maxLength: 9999999}),
	codegen = require('./codegen.js'),
	fs = require('fs');	

var javaTypeMappings = {
	'Number': 'Long'
}
	
module.exports = {
	
	render: function(mockupddModel, callback) {
		
		//var mockupddModel = deepExtend({}, mockupddModel);
		mockupddModel.classes.forEach(function(klass) {
			klass.attributes.forEach(function(attribute) {
				codegen.makeUpper(attribute, 'name');
				if (!attribute.type) {
					attribute.type = 'String';
				} else if (javaTypeMappings[attribute.type]) {
					attribute.type = javaTypeMappings[attribute.type];
				}
			});
			klass.associations.forEach(function(association) {
				codegen.makeUpper(association, 'name');
			});
		});
		fs.readFile('codegen/templates/classes.tl', function(err, template) {
			dust.loadSource(dust.compile(template.toString(), 'classesTemplate'));
			dust.render('classesTemplate', mockupddModel, function(err, out) {
				callback(out);
			});
		});
	}

}

